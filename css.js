function css1Time(texty, removeAllBut, butt) {

    var w = window.innerWidth;
    var h = window.innerHeight;
    document.getElementsByTagName("html")[0].style.background = "url('https://picsum.photos/" + w + "/" + h + "/?random')";

    texty.style.height = 50;
    texty.style.width = 550;
    texty.style.fontSize = 20;
    texty.style.backgroundColor = "#00000050";
    texty.setAttribute("type", "text");

    removeAllBut.setAttribute("type", "button");
    removeAllBut.setAttribute("value", "Remove All");
    removeAllBut.disabled = true;
    removeAllBut.style.height = 40;
    removeAllBut.style.width = 100;
    removeAllBut.style.marginLeft = 10;
    removeAllBut.style.marginBottom = 5;

    butt.setAttribute("type", "button");
    butt.setAttribute("value", "Add");
    butt.disabled = true;
    butt.style.height = 40;
    butt.style.width = 50;
    butt.style.marginLeft = 10;
    butt.style.marginBottom = 5;
}
arr = [];
order = [];
var count = Date.now();
function staticPart() {

    const main = $("#main");
    const list = document.getElementById("list");
    var texty = document.createElement("input");
    const butt = document.createElement("input");
    var removeAllBut = document.createElement("input");

    list.ondrop = (ev) => drop(ev);
    list.ondragover = (ev) => allowDrop(ev);
    texty.addEventListener('keyup', keyup);
    butt.addEventListener('click', () => { buttClicked(false, 123) });
    removeAllBut.addEventListener('click', removeAll);
    css1Time(texty,removeAllBut,butt);

    // appending
    main.append(texty);
    main.append(butt);
    main.append(removeAllBut);

    // if there was anything in localStorage
    if (localStorage.getItem('order') != null) {
        arr = JSON.parse(localStorage.getItem('arr'));
        order = localStorage.getItem('order').split(",");
        order.forEach(element => parseInt(element));
    }
    // load everything(same as button clicked(without animations))
    for (var i = 0; i < order.length; i += 1) {
        id = order[i];
        for (var j = 0; j < order.length; j += 1) {
            if (arr[j].id == order[i]) {
                buttClicked(true, id);
            }
        }
    }

    function keyup(e) {
        if (texty.value != '') {
            butt.disabled = false;
            texty.style.backgroundColor = "#ffffff";
            if (e.keyCode === 13) {
                buttClicked(false, 123);
            }
        } else {
            texty.style.backgroundColor = "#00000050";
            butt.disabled = true;
        }

    }

    function buttClicked(older, newer) {

        let newdiv = document.createElement("div");
        var buttRemove = document.createElement("img");

        buttRemove.src = "cross.png";
        buttRemove.setAttribute("visibility", "False");
        buttRemove.style.visibility = "hidden";
        buttRemove.style.height = 20;
        buttRemove.style.width = 20;
        buttRemove.style.marginTop = 5;
        buttRemove.style.marginBottom = 5;
        buttRemove.style.marginLeft = 20;
        buttRemove.style.marginRight = 5;
        buttRemove.classList.add("buttRemove");
        newdiv.style.backgroundColor = "#ffffff70";
        newdiv.style.fontSize = 25;
        newdiv.style.marginTop = 10;
        newdiv.classList.add("newdiv");
        // newdiv.ondragenter = dragenter(event);
        // newdiv.ondragstart = dragstart(event);
        // newdiv.addEventListener('ondragenter', () => { dragenter(event) });
        newdiv.addEventListener('   ', () => { drag(ev) });
        newdiv.ondragstart = (ev) => drag(ev);
        newdiv.draggable = true;
        removeAllBut.disabled = false;
        // textField.ondblclick=function(){
        //     textField.style.backgroundColor="#ffffff";
        //     textField.contentEditable=true;
        // };

        newdiv.onmouseenter = function () { buttRemove.style.visibility = "visible"; newdiv.style.backgroundColor = "#ffffffcf"; };
        newdiv.onmouseleave = function () { buttRemove.style.visibility = "hidden"; newdiv.style.backgroundColor = "#ffffff70"; };
        buttRemove.onclick = buttRemoveClicked;

        function buttRemoveClicked(e) {
            var ids = e.target.id;
            console.log(ids);
            ids = ids.split("-")[1];
            console.log(ids);
            var specimen = document.getElementById(ids);
            console.log(specimen);
            var count = -1;
            arr.forEach(function (element) {
                count += 1;//possible error
                if (element.id == ids) {
                    if (element.done) {
                        var i = setInterval(frame, 10);
                        var pos = 0;
                        function frame() {
                            if (pos == 100) {
                                clearInterval(i);
                                specimen.remove();
                            } else {
                                pos++;
                                specimen.style.marginLeft = 2 * pos;
                                specimen.style.opacity = 1 - (pos / 100);
                            }
                        }
                        arr.splice(count, 1);
                        order.splice(count, 1);
                        save();
                        if (order.length == 0) {
                            removeAllBut.disabled = true;
                        }
                    }
                    else {
                        alert("Complete the task first");
                    }
                }
            });
        }

        var p = document.createElement("br");
        var check = document.createElement("input");

        check.setAttribute("type", "checkbox");
        check.style.marginBottom = 10;
        check.style.marginLeft = 10;
        check.style.marginRight = 10;
        check.addEventListener('click', checkChecked);
        var textField = document.createElement("div");
        textField.style.wordWrap = 'break-word';
        textField.classList.add("textField");
        textField.onclick = function (e) {
            textField.style.backgroundColor = "#ffffff";
            textField.contentEditable = true;
            textField.onblur = function () {
                textField.contentEditable = false;
                textField.style.backgroundColor = "#00000000";
                var check_id = e.target.id;
                var div_id = check_id.split("-")[1];
                arr.forEach(element => {
                    if (element.id == div_id) {
                        element.todo = document.getElementById("text-" + div_id).innerHTML;
                        console.log(element.todo);
                        // console.log();
                    }
                });
                save();
            };
        };



        function checkChecked(e) {
            var check_id = e.target.id;
            var div_id = check_id.split("-")[1];
            arr.forEach(function (element) {
                if (element.id == div_id) {
                    if (element.done) {
                        element.done = false;
                        document.getElementById(div_id).style.setProperty("text-decoration", "none");
                        save();
                    }
                    else {
                        element.done = true;
                        document.getElementById(div_id).style.setProperty("text-decoration", "line-through");
                        save();
                    }
                }
            });
        }

        if (older == true) {
            check.checked = arr[j].done;
            if (check.checked) { newdiv.style.setProperty("text-decoration", "line-through"); }
            textField.append(arr[j].todo);
        } else {
            textField.append(texty.value);
        }
        newdiv.append(check);
        newdiv.append(textField);
        newdiv.append(buttRemove);
        newdiv.append(p);

        // animate new
        var i = setInterval(frameAdd, 10);
        var pos = 100;
        function frameAdd() {
            if (pos == 0) {
                clearInterval(i);
            } else {
                pos--;
                newdiv.style.marginTop = 10 + pos;
                newdiv.style.opacity = 1 - (pos / 100);
            }
        }

        list.append(newdiv);

        //id's
        if (older == true) {
            newdiv.setAttribute("id", newer);
            check.setAttribute("id", "check-" + newer);
            textField.setAttribute("id", "text-" + newer);
            buttRemove.setAttribute("id", "del-" + newer);
        } else {
            var id = count++;
            newdiv.setAttribute("id", id);
            check.setAttribute("id", "check-" + id);
            textField.setAttribute("id", "text-" + id);
            buttRemove.setAttribute("id", "del-" + id);

            //Data Structure
            data_obj = {
                id: id,
                done: check.checked,
                todo: texty.value,
            };

            arr.push(data_obj);
            order.push(id);
            save();
            texty.value = '';
            texty.style.backgroundColor = "#00000050";
            butt.disabled = true;
        }
        console.log(arr);
        console.log(order);
    }

    function save() {
        localStorage.setItem("arr", JSON.stringify(arr));
        localStorage.setItem("order", order);
    }

    function allowDrop(ev) {
        ev.preventDefault();
    }

    function drag(ev) {
        ev.dataTransfer.setData("text", ev.target.id);
    }

    function drop(ev) {
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");

        thisdiv = ev.target;
        var newElement = document.getElementById(data);
        var targetElement = thisdiv;
        var parent = targetElement.parentNode;
        var found = false;
        for (var i = 0; i < order.length - 1; i += 1) {
            if (document.getElementById(data).id == order[i]) {
                found = true;
            }
            if (found) {
                order[i] = order[i + 1];
            }
        }
        for (var i = order.length - 1; i > 0; i -= 1) {
            if (thisdiv.id == order[i - 1]) {
                found = false;
            }
            if (found) {
                order[i] = order[i - 1];
            } else {
                order[i] = document.getElementById(data).id;
                break;
            }
        }
        console.log(order);
        save();

        if (parent.lastChild == targetElement) {
            parent.appendChild(newElement);
        } else {
            parent.insertBefore(newElement, targetElement.nextSibling);
        }
    }

    function removeAll() {
        if (confirm("Are you sure, you want to delete all to-do's?")) {
            var temp = -1;
            order.forEach(element => {
                var len = order.length;
                var specifictemp = temp += 1;
                var specimen = document.getElementById(element);
                var i = setInterval(frameAllRemove, 10);
                var pos = 0;
                function frameAllRemove() {
                    setTimeout(function () {
                        if (pos == 100) {
                            clearInterval(i);
                            setTimeout(function () { specimen.remove(); }, (len * 100));
                        } else {
                            pos++;
                            specimen.style.marginLeft = 2 * pos;
                            specimen.style.opacity = 1 - (pos / 100);
                        }

                    }, specifictemp * 100);
                }
            });
            localStorage.clear();
            removeAllBut.disabled = true;
            arr = [];
            order = [];
        }
    }
}